# ////////////
# // locals //
# ////////////
# locals {
#   var_enable_endpoints_parsed = [for endpoint in var.enable_endpoints : replace(replace(lower(endpoint), "-", ""), ".", "")]
#   var_endpoint_setup          = [for endpoint in local.endpoints_formal_names : endpoint if contains(local.var_enable_endpoints_parsed, replace(replace(lower(endpoint), "-", ""), ".", ""))]

#   endpoints_formal_names = [
#     "s3",
#     "codebuild",
#     "codecommit",
#     "git-codecommit",
#     "config",
#     "sqs",
#     "lambda",
#     "secretsmanager",
#     "ssm",
#     "ssmmessages",
#     "ec2",
#     "ec2messages",
#     "autoscaling",
#     "transfer.server",
#     "ecr.api",
#     "ecr.dkr",
#     "execute-api",
#     "kms",
#     "ecs",
#     "ecs-agent",
#     "ecs-telemetry",
#     "sns",
#     "monitoring",
#     "logs",
#     "events",
#     "elasticloadbalancing",
#     "cloudtrail",
#     "kinesis-streams",
#     "kinesis-firehose",
#     "glue",
#     "notebook",
#     "sts",
#     "cloudformation",
#     "codepipeline",
#     "appmesh-envoy-management",
#     "servicecatalog",
#     "storagegateway",
#     "transfer",
#     "sagemaker.api",
#     "sagemaker.runtime",
#     "appstream.api",
#     "appstream.streaming",
#     "athena",
#     "rekognition",
#     "elasticfilesystem",
#     "clouddirectory",
#     "autoscaling-plans",
#     "workspaces",
#     "access-analyzer",
#     "ebs",
#     "datasync",
#     "elastic-inference.runtime",
#     "sms",
#     "elasticmapreduce",
#     "qldb.session",
#     "states",
#     "elasticbeanstalk",
#     "elasticbeanstalk-health",
#     "acm-pca",
#     "email-smtp",
#     "rds",
#     "codedeploy",
#     "codedeploy-commands-secure",
#     "textract",
#     "codeartifact.api",
#     "codeartifact.repositories"
#   ]
# }

# /////////////////////////
# // interface endpoints //
# /////////////////////////
# data "aws_vpc_endpoint_service" "this" {
#   for_each = toset(local.var_endpoint_setup)

#   service      = each.value
#   service_type = "Interface"
# }

# resource "aws_vpc_endpoint" "this" {
#   for_each = toset(local.var_endpoint_setup)

#   vpc_id            = aws_vpc.this[0].id
#   service_name      = data.aws_vpc_endpoint_service.this[0].service_name
#   vpc_endpoint_type = "Interface"

#   security_group_ids = [aws_security_group.default["this"].id]
#   subnet_ids         = aws_subnet.private.*.id
#   tags               = merge(var.tags, local.tags, { Name = "${var.name}-endpoint-${replace(replace(lower(each.value), "-", ""), ".", "")}" })
# }

# /////////////////////
# // security groups //
# /////////////////////
# resource "aws_security_group" "this" {
#   for_each    = toset(local.var_endpoint_setup)
#   name        = "endpoint-sg-${replace(replace(lower(each.value), "-", ""), ".", "")}"
#   description = "Allow all inbound traffic"
#   vpc_id      = aws_vpc.this[0].id

#   ingress {
#     from_port        = 0
#     to_port          = 0
#     protocol         = "-1"
#     cidr_blocks      = ["0.0.0.0/0"]
#     ipv6_cidr_blocks = ["::/0"]
#   }

#   egress {
#     from_port        = 0
#     to_port          = 0
#     protocol         = "-1"
#     cidr_blocks      = ["0.0.0.0/0"]
#     ipv6_cidr_blocks = ["::/0"]
#   }

#   tags = merge(var.tags, local.tags, { Name = "endpoint-sg-${replace(replace(lower(each.value), "-", ""), ".", "")}" })
# }

# ///////////////////////
# // gateway endpoints //
# ///////////////////////
# //s3
# data "aws_vpc_endpoint_service" "s3gateway" {
#   count = contains(local.var_enable_endpoints_parsed, "s3gateway") ? 1 : 0

#   service      = "s3"
#   service_type = "Gateway"
# }

# resource "aws_vpc_endpoint" "s3gateway" {
#   count = contains(local.var_enable_endpoints_parsed, "s3gateway") ? 1 : 0

#   vpc_id       = aws_vpc.this[0].id
#   service_name = data.aws_vpc_endpoint_service.s3gateway[0].service_name
#   tags         = merge(var.tags, local.tags, { Name = "${var.name}-endpoint-s3gateway" })
# }

# resource "aws_vpc_endpoint_route_table_association" "private_s3" {
#   count = contains(local.var_enable_endpoints_parsed, "s3gateway") ? length(aws_route_table.private) : 0

#   vpc_endpoint_id = aws_vpc_endpoint.s3gateway[0].id
#   route_table_id  = element(aws_route_table.private.*.id, count.index)
# }

# resource "aws_vpc_endpoint_route_table_association" "protected_s3" {
#   count = contains(local.var_enable_endpoints_parsed, "s3gateway") ? length(aws_route_table.protected) : 0

#   vpc_endpoint_id = aws_vpc_endpoint.s3gateway[0].id
#   route_table_id  = element(aws_route_table.protected.*.id, count.index)
# }

# //dynamodb
# data "aws_vpc_endpoint_service" "dynamodbgateway" {
#   count = contains(local.var_enable_endpoints_parsed, "dynamodbgateway") ? 1 : 0

#   service      = "dynamodb"
#   service_type = "Gateway"
# }

# resource "aws_vpc_endpoint" "dynamodbgateway" {
#   count = contains(local.var_enable_endpoints_parsed, "dynamodbgateway") ? 1 : 0

#   vpc_id       = aws_vpc.this[0].id
#   service_name = data.aws_vpc_endpoint_service.dynamodbgateway[0].service_name
#   tags         = merge(var.tags, local.tags, { Name = "${var.name}-endpoint-dynamodbgateway" })
# }

# resource "aws_vpc_endpoint_route_table_association" "private_dynamodb" {
#   count = contains(local.var_enable_endpoints_parsed, "dynamodbgateway") ? length(aws_route_table.private) : 0

#   vpc_endpoint_id = aws_vpc_endpoint.dynamodbgateway[0].id
#   route_table_id  = element(aws_route_table.private.*.id, count.index)
# }

# resource "aws_vpc_endpoint_route_table_association" "protected_dynamodb" {
#   count = contains(local.var_enable_endpoints_parsed, "dynamodbgateway") ? length(aws_route_table.protected) : 0

#   vpc_endpoint_id = aws_vpc_endpoint.dynamodbgateway[0].id
#   route_table_id  = element(aws_route_table.protected.*.id, count.index)
# }
