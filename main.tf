/*
VPC
*/
resource "aws_vpc" "this" {
  count = var.vpc_cidr != "" ? 1 : 0

  cidr_block = var.vpc_cidr

  enable_dns_support   = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames

  tags = merge(local.tags, {
    Name = lower(var.name)
  })
}

/*
DHCP Options Set
*/
resource "aws_vpc_dhcp_options" "this" {
  count = var.enable_dhcp_options ? 1 : 0

  domain_name          = var.dhcp_options_domain_name
  domain_name_servers  = var.dhcp_options_domain_name_servers
  ntp_servers          = var.dhcp_options_ntp_servers
  netbios_name_servers = var.dhcp_options_netbios_name_servers
  netbios_node_type    = var.dhcp_options_netbios_node_type

  tags = merge(local.tags, {
    Name = lower(var.name)
  })
}

/*
DHCP Options Set Association
*/
resource "aws_vpc_dhcp_options_association" "this" {
  count = var.enable_dhcp_options ? 1 : 0

  vpc_id          = aws_vpc.this[0].id
  dhcp_options_id = aws_vpc_dhcp_options.this[0].id
}

/*
Internet Gateway
*/
resource "aws_internet_gateway" "this" {
  count = var.create_vpc_internet_gateway && length(var.public_subnets) > 0 ? 1 : 0

  vpc_id = aws_vpc.this[0].id

  tags = merge(local.tags, {
    Name = lower(var.name)
  })
}

/*
Public subnets
*/
resource "aws_subnet" "public" {
  count = length(var.public_subnets) > 0 ? length(local.var_availability_zones) : 0

  vpc_id                  = aws_vpc.this[0].id
  availability_zone       = element(local.var_availability_zones, count.index)
  cidr_block              = element(var.public_subnets, count.index)
  map_public_ip_on_launch = true

  depends_on = [aws_vpc.this]

  tags = merge(local.tags, {
    Name = lower("${var.name}-public-${element(local.var_availability_zones, count.index)}")
  })
}

resource "aws_route_table" "public" {
  count = length(var.public_subnets) > 0 ? 1 : 0

  vpc_id = aws_vpc.this[0].id

  tags = merge(local.tags, {
    Name = lower("${var.name}-public")
  })

  depends_on = [aws_subnet.public]
}

resource "aws_route_table_association" "public" {
  count = length(var.public_subnets) > 0 ? length(local.var_availability_zones) : 0

  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public.0.id

  depends_on = [aws_route_table.public, aws_subnet.public]
}

resource "aws_route" "public_internet_gateway" {
  count = length(var.public_subnets) > 0 ? 1 : 0

  route_table_id         = aws_route_table.public.0.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.this[0].id

  depends_on = [aws_route_table.public]
}

/*
# Public Network ACLs
*/
resource "aws_network_acl" "public" {
  count = var.public_dedicated_network_acl && length(var.public_subnets) > 0 ? 1 : 0

  vpc_id     = element(concat(aws_vpc.this.*.id, [""]), 0)
  subnet_ids = aws_subnet.public.*.id

  tags = merge(local.tags, {
    Name = lower("${var.name}-public")
  })
}

resource "aws_network_acl_rule" "public_inbound" {
  count = var.public_dedicated_network_acl && length(var.public_subnets) > 0 ? length(var.public_inbound_acl_rules) : 0

  network_acl_id = aws_network_acl.public[0].id

  egress          = false
  rule_number     = var.public_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.public_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.public_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.public_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.public_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.public_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.public_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.public_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.public_inbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "public_outbound" {
  count = var.public_dedicated_network_acl && length(var.public_subnets) > 0 ? length(var.public_outbound_acl_rules) : 0

  network_acl_id = aws_network_acl.public[0].id

  egress          = true
  rule_number     = var.public_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.public_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.public_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.public_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.public_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.public_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.public_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.public_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.public_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

/*
Private subnets
*/
resource "aws_subnet" "private" {
  count = length(var.private_subnets) > 0 ? length(local.var_availability_zones) : 0

  vpc_id                  = aws_vpc.this[0].id
  availability_zone       = element(local.var_availability_zones, count.index)
  cidr_block              = element(var.private_subnets, count.index)
  map_public_ip_on_launch = false

  tags = merge(local.tags, {
    Name = lower("${var.name}-private-${element(local.var_availability_zones, count.index)}")
  })

  depends_on = [aws_vpc.this]
}

resource "aws_route_table" "private" {
  count = length(var.private_subnets) > 0 ? length(local.var_availability_zones) : 0

  vpc_id = aws_vpc.this[0].id

  tags = merge(local.tags, {
    Name = lower("${var.name}-private-${element(local.var_availability_zones, count.index)}")
  })

  depends_on = [aws_subnet.private]
}

resource "aws_route_table_association" "private" {
  count = length(var.private_subnets) > 0 ? length(local.var_availability_zones) : 0

  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}

/*
# Private Network ACLs
*/
resource "aws_network_acl" "private" {
  count = var.private_dedicated_network_acl && length(var.private_subnets) > 0 ? 1 : 0

  vpc_id     = element(concat(aws_vpc.this.*.id, [""]), 0)
  subnet_ids = aws_subnet.private.*.id

  tags = merge(local.tags, {
    Name = lower("${var.name}-private")
  })
}

resource "aws_network_acl_rule" "private_inbound" {
  count = var.private_dedicated_network_acl && length(var.private_subnets) > 0 ? length(var.private_inbound_acl_rules) : 0

  network_acl_id = aws_network_acl.private[0].id

  egress          = false
  rule_number     = var.private_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.private_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.private_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.private_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.private_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.private_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.private_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.private_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.private_inbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "private_outbound" {
  count = var.private_dedicated_network_acl && length(var.private_subnets) > 0 ? length(var.private_outbound_acl_rules) : 0

  network_acl_id = aws_network_acl.private[0].id

  egress          = true
  rule_number     = var.private_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.private_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.private_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.private_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.private_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.private_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.private_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.private_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.private_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

/*
Protected subnets
*/
resource "aws_subnet" "protected" {
  count = length(var.protected_subnets) > 0 ? length(local.var_availability_zones) : 0

  vpc_id                  = aws_vpc.this[0].id
  availability_zone       = element(local.var_availability_zones, count.index)
  cidr_block              = element(var.protected_subnets, count.index)
  map_public_ip_on_launch = false

  tags = merge(local.tags, {
    Name = lower("${var.name}-protected-${element(local.var_availability_zones, count.index)}")
  })

  depends_on = [aws_vpc.this]
}

resource "aws_route_table" "protected" {
  count = length(var.protected_subnets) > 0 ? 1 : 0

  vpc_id = aws_vpc.this[0].id

  tags = merge(local.tags, {
    Name = lower("${var.name}-protected")
  })

  depends_on = [aws_subnet.protected]
}

resource "aws_route_table_association" "protected" {
  count = length(var.protected_subnets) > 0 ? length(local.var_availability_zones) : 0

  subnet_id      = element(aws_subnet.protected.*.id, count.index)
  route_table_id = aws_route_table.protected.0.id
}

/*
# Protected Network ACLs
*/
resource "aws_network_acl" "protected" {
  count = var.protected_dedicated_network_acl && length(var.protected_subnets) > 0 ? 1 : 0

  vpc_id     = element(concat(aws_vpc.this.*.id, [""]), 0)
  subnet_ids = aws_subnet.protected.*.id

  tags = merge(local.tags, {
    Name = lower("${var.name}-protected")
  })
}

resource "aws_network_acl_rule" "protected_inbound" {
  count = var.protected_dedicated_network_acl && length(var.protected_subnets) > 0 ? length(var.protected_inbound_acl_rules) : 0

  network_acl_id = aws_network_acl.protected[0].id

  egress          = false
  rule_number     = var.protected_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.protected_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.protected_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.protected_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.protected_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.protected_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.protected_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.protected_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.protected_inbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "protected_outbound" {
  count = var.protected_dedicated_network_acl && length(var.protected_subnets) > 0 ? length(var.protected_outbound_acl_rules) : 0

  network_acl_id = aws_network_acl.protected[0].id

  egress          = true
  rule_number     = var.protected_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.protected_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.protected_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.protected_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.protected_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.protected_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.protected_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.protected_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.protected_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

/*
Routing Subnets via Transit Gateway
*/
resource "aws_route" "private_transit_gateway_routing" {
  count = var.transit_gateway_routing == true && length(var.private_subnets) > 0 ? length(local.var_availability_zones) : 0

  route_table_id         = element(aws_route_table.private.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  transit_gateway_id     = var.transit_gateway_id
}

resource "aws_route" "public_transit_gateway_routing" {
  count = var.transit_gateway_routing == true && length(var.public_subnets) > 0 ? length(var.private_cidrs) : 0

  route_table_id         = aws_route_table.public[0].id
  destination_cidr_block = var.private_cidrs[count.index]
  transit_gateway_id     = var.transit_gateway_id
}

/*
VPC Flow Logs
*/
resource "aws_cloudwatch_log_group" "this" {
  count = var.create_vpc_flow_logs ? 1 : 0

  name              = "/aws/vpc/flow_logs/${lower(var.name)}"
  retention_in_days = "90"

  tags = merge(local.tags, {
    Name = lower("${var.name}-")
  })
}

resource "aws_iam_role" "this" {
  count = var.create_vpc_flow_logs ? 1 : 0

  name = "${upper(var.name)}-VPC-CloudWatchLogRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

  tags = merge(local.tags, {
    Name = upper(var.name)
  })
}

resource "aws_iam_role_policy" "this" {
  count = var.create_vpc_flow_logs ? 1 : 0

  name   = "${upper(var.name)}-VPC-CloudWatchLogPolicy"
  role   = aws_iam_role.this[0].id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_flow_log" "this" {
  count = var.create_vpc_flow_logs ? 1 : 0

  iam_role_arn    = aws_iam_role.this[0].arn
  log_destination = aws_cloudwatch_log_group.this[0].arn
  traffic_type    = "ALL"
  vpc_id          = aws_vpc.this[0].id

  tags = merge(local.tags, {
    Name = lower(var.name)
  })
}

/*
NAT Gateways
*/
resource "aws_eip" "nat" {
  count = var.create_vpc_nat_gateways && length(var.public_subnets) > 0 ? length(local.var_availability_zones) : 0

  vpc = true

  tags = merge(local.tags, {
    Name = lower("${var.name}-eip-${element(local.var_availability_zones, count.index)}")
  })
}

resource "aws_nat_gateway" "this" {
  count = var.create_vpc_nat_gateways && length(var.public_subnets) > 0 ? length(local.var_availability_zones) : 0

  allocation_id = element(aws_eip.nat.*.id, count.index)
  subnet_id     = element(aws_subnet.public.*.id, count.index)

  tags = merge(local.tags, {
    Name = lower("${var.name}-nat-${element(local.var_availability_zones, count.index)}")
  })
}

resource "aws_route" "private_nat_gateway" {
  count = var.create_vpc_nat_gateways && length(var.private_subnets) > 0 ? length(local.var_availability_zones) : 0

  route_table_id         = element(aws_route_table.private.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.this.*.id, count.index)

  timeouts {
    create = "5m"
  }
}


/* Transit Gateway Attachment */
resource "aws_ec2_transit_gateway_vpc_attachment" "this_vpc_tgw_attachment" {

  count = var.create_transit_gateway_attachment == true ? 1 : 0

  transit_gateway_id = var.transit_gateway_id
  vpc_id             = aws_vpc.this.0.id

  // Usually we expect people to want to make attachments to the private subnets.
  // If there are no private subnets, we take the public ones, otherwise an attachment doesn't make sense.
  subnet_ids = length(var.private_subnets) > 0 ? aws_subnet.private.*.id : aws_subnet.public.*.id

  tags = merge(local.tags, {
    Name = "${lower(var.name)}-to-${var.transit_gateway_id}"
  })

}
