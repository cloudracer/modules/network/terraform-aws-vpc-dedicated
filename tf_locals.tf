locals {
  var_enable_endpoints_parsed = [for endpoint in var.enable_endpoints : replace(replace(lower(endpoint), "-", ""), ".", "")]
  var_security_groups         = compact([for endpoint in local.var_enable_endpoints_parsed : endpoint == "s3" || endpoint == "dynamodb" ? "" : endpoint])
  var_availability_zones      = slice(data.aws_availability_zones.current.names, "0", var.availability_zones)

  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-vpc-dedicated"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/terraform-aws-vpc-dedicated"
  }
  tags = merge(local.tags_module, var.tags)
}
