output "vpc_id" {
  description = "The ID of the VPC"
  value       = concat(aws_vpc.this.*.id, [""])[0]
}
output "igw_id" {
  description = "The ID of the Internet Gateway"
  value       = concat(aws_internet_gateway.this.*.id, [""])[0]
}
output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = aws_subnet.public.*.id
}
output "public_route_table_ids" {
  description = "List of IDs of public route tables"
  value       = aws_route_table.public.*.id
}
output "public_subnets_cidr_blocks" {
  description = "List of cidr_blocks of public subnets"
  value       = aws_subnet.public.*.cidr_block
}
output "private_subnets" {
  description = "List of IDs of private subnets"
  value       = aws_subnet.private.*.id
}
output "private_route_table_ids" {
  description = "List of IDs of private route tables"
  value       = aws_route_table.private.*.id
}
output "private_subnets_cidr_blocks" {
  description = "List of cidr_blocks of private subnets"
  value       = aws_subnet.private.*.cidr_block
}
output "protected_subnets" {
  description = "List of IDs of protected subnets"
  value       = aws_subnet.protected.*.id
}
output "protected_route_table_ids" {
  description = "List of IDs of protected route tables"
  value       = aws_route_table.protected.*.id
}
output "protected_subnets_cidr_blocks" {
  description = "List of cidr_blocks of protected subnets"
  value       = aws_subnet.protected.*.cidr_block
}
output "nat_gateway_ids" {
  description = "List of NAT Gateway IDs"
  value       = aws_nat_gateway.this.*.id
}
output "nat_gateway_public_ips" {
  description = "List of public Elastic IPs created for AWS NAT Gateway"
  value       = aws_eip.nat.*.public_ip
}
output "vpc" {
  description = ""
  value       = aws_vpc.this[0]
}
output "public_subnet" {
  description = ""
  value       = aws_subnet.public
}
output "private_subnet" {
  description = ""
  value       = aws_subnet.private
}
output "protected_subnet" {
  description = ""
  value       = aws_subnet.protected
}
