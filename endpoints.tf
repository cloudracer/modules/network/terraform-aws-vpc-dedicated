######################
# Security Groups (default) for Interface Endpoints
######################
resource "aws_security_group" "default" {
  for_each    = toset(local.var_security_groups)
  name        = "endpoint-sg-${each.key}"
  description = "Allow all inbound traffic"
  vpc_id      = aws_vpc.this[0].id

  ingress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = merge(local.tags, { Name = "endpoint-sg-${each.key}" })
}

######################
# VPC Endpoint for S3 (Gateway-Endpoint)
######################
data "aws_vpc_endpoint_service" "s3" {
  count = contains(local.var_enable_endpoints_parsed, "s3") ? 1 : 0

  service      = "s3"
  service_type = "Gateway"
}

resource "aws_vpc_endpoint" "s3" {
  count = contains(local.var_enable_endpoints_parsed, "s3") ? 1 : 0

  vpc_id       = aws_vpc.this[0].id
  service_name = data.aws_vpc_endpoint_service.s3[0].service_name
  tags         = merge(local.tags, { Name = "${var.name}-endpoint-s3" })
}

resource "aws_vpc_endpoint_route_table_association" "private_s3" {
  count = contains(local.var_enable_endpoints_parsed, "s3") ? length(aws_route_table.private) : 0

  vpc_endpoint_id = aws_vpc_endpoint.s3[0].id
  route_table_id  = element(aws_route_table.private.*.id, count.index)
}

resource "aws_vpc_endpoint_route_table_association" "protected_s3" {
  count = contains(local.var_enable_endpoints_parsed, "s3") ? length(aws_route_table.protected) : 0

  vpc_endpoint_id = aws_vpc_endpoint.s3[0].id
  route_table_id  = element(aws_route_table.protected.*.id, count.index)
}

############################
# VPC Endpoint for DynamoDB (Gateway-Endpoint)
############################
data "aws_vpc_endpoint_service" "dynamodb" {
  count = contains(local.var_enable_endpoints_parsed, "dynamodb") ? 1 : 0

  service      = "dynamodb"
  service_type = "Gateway"
}

resource "aws_vpc_endpoint" "dynamodb" {
  count = contains(local.var_enable_endpoints_parsed, "dynamodb") ? 1 : 0

  vpc_id       = aws_vpc.this[0].id
  service_name = data.aws_vpc_endpoint_service.dynamodb[0].service_name
  tags         = merge(local.tags, { Name = "${var.name}-endpoint-dynamodb" })
}

resource "aws_vpc_endpoint_route_table_association" "private_dynamodb" {
  count = contains(local.var_enable_endpoints_parsed, "dynamodb") ? length(aws_route_table.private) : 0

  vpc_endpoint_id = aws_vpc_endpoint.dynamodb[0].id
  route_table_id  = element(aws_route_table.private.*.id, count.index)
}

resource "aws_vpc_endpoint_route_table_association" "protected_dynamodb" {
  count = contains(local.var_enable_endpoints_parsed, "dynamodb") ? length(aws_route_table.protected) : 0

  vpc_endpoint_id = aws_vpc_endpoint.dynamodb[0].id
  route_table_id  = element(aws_route_table.protected.*.id, count.index)
}

#############################
# VPC Endpoint for S3 (Interface-Endpoint)
#############################
data "aws_vpc_endpoint_service" "s3interface" {
  count = contains(local.var_enable_endpoints_parsed, "s3interface") ? 1 : 0 //

  service      = "s3"
  service_type = "Interface"
}

resource "aws_vpc_endpoint" "s3interface" {
  count = contains(local.var_enable_endpoints_parsed, "s3interface") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.s3interface[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids = [aws_security_group.default["s3interface"].id]                   // Klammern
  subnet_ids         = aws_subnet.private.*.id                                          // private ids
  tags               = merge(local.tags, { Name = "${var.name}-endpoint-s3interface" }) // name hinzufügen
}

#############################
# VPC Endpoint for Codebuild
#############################
data "aws_vpc_endpoint_service" "codebuild" {
  count = contains(local.var_enable_endpoints_parsed, "codebuild") ? 1 : 0

  service = "codebuild"
}

resource "aws_vpc_endpoint" "codebuild" {
  count = contains(local.var_enable_endpoints_parsed, "codebuild") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.codebuild[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["codebuild"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-codebuild" })
}

###############################
# VPC Endpoint for Code Commit
###############################
data "aws_vpc_endpoint_service" "codecommit" {
  count = contains(local.var_enable_endpoints_parsed, "codecommit") ? 1 : 0

  service = "codecommit"
}

resource "aws_vpc_endpoint" "codecommit" {
  count = contains(local.var_enable_endpoints_parsed, "codecommit") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.codecommit[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["codecommit"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-codecommit" })
}

###################################
# VPC Endpoint for Git Code Commit
###################################
data "aws_vpc_endpoint_service" "git_codecommit" {
  count = contains(local.var_enable_endpoints_parsed, "gitcodecommit") ? 1 : 0

  service = "git-codecommit"
}

resource "aws_vpc_endpoint" "git_codecommit" {
  count = contains(local.var_enable_endpoints_parsed, "gitcodecommit") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.git_codecommit[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["gitcodecommit"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-gitcodecommit" })
}

##########################
# VPC Endpoint for Config
##########################
data "aws_vpc_endpoint_service" "config" {
  count = contains(local.var_enable_endpoints_parsed, "config") ? 1 : 0

  service = "config"
}

resource "aws_vpc_endpoint" "config" {
  count = contains(local.var_enable_endpoints_parsed, "config") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.config[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["config"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-config" })
}

#######################
# VPC Endpoint for SQS
#######################
data "aws_vpc_endpoint_service" "sqs" {
  count = contains(local.var_enable_endpoints_parsed, "sqs") ? 1 : 0

  service = "sqs"
}

resource "aws_vpc_endpoint" "sqs" {
  count = contains(local.var_enable_endpoints_parsed, "sqs") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.sqs[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["sqs"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-sqs" })
}

#########################
# VPC Endpoint for Lambda
#########################
data "aws_vpc_endpoint_service" "lambda" {
  count = contains(local.var_enable_endpoints_parsed, "lambda") ? 1 : 0

  service = "lambda"
}
resource "aws_vpc_endpoint" "lambda" {
  count = contains(local.var_enable_endpoints_parsed, "lambda") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.lambda[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["lambda"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-lambda" })
}

###################################
# VPC Endpoint for Secrets Manager
###################################
data "aws_vpc_endpoint_service" "secretsmanager" {
  count = contains(local.var_enable_endpoints_parsed, "secretsmanager") ? 1 : 0

  service = "secretsmanager"
}

resource "aws_vpc_endpoint" "secretsmanager" {
  count = contains(local.var_enable_endpoints_parsed, "secretsmanager") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.secretsmanager[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["secretsmanager"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-secretsmanager" })
}

#######################
# VPC Endpoint for SSM
#######################
data "aws_vpc_endpoint_service" "ssm" {
  count = contains(local.var_enable_endpoints_parsed, "ssm") ? 1 : 0

  service = "ssm"
}

resource "aws_vpc_endpoint" "ssm" {
  count = contains(local.var_enable_endpoints_parsed, "ssm") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.ssm[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["ssm"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-ssm" })
}

###############################
# VPC Endpoint for SSMMESSAGES
###############################
data "aws_vpc_endpoint_service" "ssmmessages" {
  count = contains(local.var_enable_endpoints_parsed, "ssmmessages") ? 1 : 0

  service = "ssmmessages"
}

resource "aws_vpc_endpoint" "ssmmessages" {
  count = contains(local.var_enable_endpoints_parsed, "ssmmessages") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.ssmmessages[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["ssmmessages"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-ssmmessages" })
}

#######################
# VPC Endpoint for EC2
#######################
data "aws_vpc_endpoint_service" "ec2" {
  count = contains(local.var_enable_endpoints_parsed, "ec2") ? 1 : 0

  service = "ec2"
}

resource "aws_vpc_endpoint" "ec2" {
  count = contains(local.var_enable_endpoints_parsed, "ec2") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.ec2[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["ec2"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-ec2" })
}

###############################
# VPC Endpoint for EC2MESSAGES
###############################
data "aws_vpc_endpoint_service" "ec2messages" {
  count = contains(local.var_enable_endpoints_parsed, "ec2messages") ? 1 : 0

  service = "ec2messages"
}

resource "aws_vpc_endpoint" "ec2messages" {
  count = contains(local.var_enable_endpoints_parsed, "ec2messages") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.ec2messages[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["ec2messages"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-ec2messages" })
}

###############################
# VPC Endpoint for EC2 Autoscaling
###############################
data "aws_vpc_endpoint_service" "ec2_autoscaling" {
  count = contains(local.var_enable_endpoints_parsed, "autoscaling") ? 1 : 0

  service = "autoscaling"
}

resource "aws_vpc_endpoint" "ec2_autoscaling" {
  count = contains(local.var_enable_endpoints_parsed, "autoscaling") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.ec2_autoscaling[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["autoscaling"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-autoscaling" })
}

###################################
# VPC Endpoint for Transfer Server
###################################
data "aws_vpc_endpoint_service" "transferserver" {
  count = contains(local.var_enable_endpoints_parsed, "transferserver") ? 1 : 0

  service = "transfer.server"
}

resource "aws_vpc_endpoint" "transferserver" {
  count = contains(local.var_enable_endpoints_parsed, "transferserver") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.transferserver[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["transferserver"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-transferserver" })
}

###########################
# VPC Endpoint for ECR API
###########################
data "aws_vpc_endpoint_service" "ecr_api" {
  count = contains(local.var_enable_endpoints_parsed, "ecrapi") ? 1 : 0

  service = "ecr.api"
}

resource "aws_vpc_endpoint" "ecr_api" {
  count = contains(local.var_enable_endpoints_parsed, "ecrapi") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.ecr_api[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["ecrapi"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-ecrapi" })
}

###########################
# VPC Endpoint for ECR DKR
###########################
data "aws_vpc_endpoint_service" "ecr_dkr" {
  count = contains(local.var_enable_endpoints_parsed, "ecrdkr") ? 1 : 0

  service = "ecr.dkr"
}

resource "aws_vpc_endpoint" "ecr_dkr" {
  count = contains(local.var_enable_endpoints_parsed, "ecrdkr") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.ecr_dkr[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["ecrdkr"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-ecrdkr" })
}

#######################
# VPC Endpoint for API Gateway
#######################
data "aws_vpc_endpoint_service" "apigw" {
  count = contains(local.var_enable_endpoints_parsed, "executeapi") ? 1 : 0

  service = "execute-api"
}

resource "aws_vpc_endpoint" "apigw" {
  count = contains(local.var_enable_endpoints_parsed, "executeapi") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.apigw[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["executeapi"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-executeapi" })
}

#######################
# VPC Endpoint for KMS
#######################
data "aws_vpc_endpoint_service" "kms" {
  count = contains(local.var_enable_endpoints_parsed, "kms") ? 1 : 0

  service = "kms"
}

resource "aws_vpc_endpoint" "kms" {
  count = contains(local.var_enable_endpoints_parsed, "kms") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.kms[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["kms"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-kms" })
}

#######################
# VPC Endpoint for ECS
#######################
data "aws_vpc_endpoint_service" "ecs" {
  count = contains(local.var_enable_endpoints_parsed, "ecs") ? 1 : 0

  service = "ecs"
}

resource "aws_vpc_endpoint" "ecs" {
  count = contains(local.var_enable_endpoints_parsed, "ecs") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.ecs[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["ecs"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-ecs" })
}

#######################
# VPC Endpoint for ECS Agent
#######################
data "aws_vpc_endpoint_service" "ecs_agent" {
  count = contains(local.var_enable_endpoints_parsed, "ecsagent") ? 1 : 0

  service = "ecs-agent"
}

resource "aws_vpc_endpoint" "ecs_agent" {
  count = contains(local.var_enable_endpoints_parsed, "ecsagent") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.ecs_agent[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["ecsagent"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-ecsagent" })
}

#######################
# VPC Endpoint for ECS Telemetry
#######################
data "aws_vpc_endpoint_service" "ecs_telemetry" {
  count = contains(local.var_enable_endpoints_parsed, "ecstelemetry") ? 1 : 0

  service = "ecs-telemetry"
}

resource "aws_vpc_endpoint" "ecs_telemetry" {
  count = contains(local.var_enable_endpoints_parsed, "ecstelemetry") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.ecs_telemetry[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["ecstelemetry"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-ecstelemetry" })
}

#######################
# VPC Endpoint for SNS
#######################
data "aws_vpc_endpoint_service" "sns" {
  count = contains(local.var_enable_endpoints_parsed, "sns") ? 1 : 0

  service = "sns"
}

resource "aws_vpc_endpoint" "sns" {
  count = contains(local.var_enable_endpoints_parsed, "sns") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.sns[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["sns"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-sns" })
}

#######################
# VPC Endpoint for CloudWatch Monitoring
#######################
data "aws_vpc_endpoint_service" "monitoring" {
  count = contains(local.var_enable_endpoints_parsed, "monitoring") ? 1 : 0

  service = "monitoring"
}

resource "aws_vpc_endpoint" "monitoring" {
  count = contains(local.var_enable_endpoints_parsed, "monitoring") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.monitoring[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["monitoring"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-monitoring" })
}

#######################
# VPC Endpoint for CloudWatch Logs
#######################
data "aws_vpc_endpoint_service" "logs" {
  count = contains(local.var_enable_endpoints_parsed, "logs") ? 1 : 0

  service = "logs"
}

resource "aws_vpc_endpoint" "logs" {
  count = contains(local.var_enable_endpoints_parsed, "logs") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.logs[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["logs"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-logs" })
}

#######################
# VPC Endpoint for CloudWatch Events
#######################
data "aws_vpc_endpoint_service" "events" {
  count = contains(local.var_enable_endpoints_parsed, "events") ? 1 : 0

  service = "events"
}

resource "aws_vpc_endpoint" "events" {
  count = contains(local.var_enable_endpoints_parsed, "events") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.events[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["events"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-events" })
}

#######################
# VPC Endpoint for Elastic Load Balancing
#######################
data "aws_vpc_endpoint_service" "elasticloadbalancing" {
  count = contains(local.var_enable_endpoints_parsed, "elasticloadbalancing") ? 1 : 0

  service = "elasticloadbalancing"
}

resource "aws_vpc_endpoint" "elasticloadbalancing" {
  count = contains(local.var_enable_endpoints_parsed, "elasticloadbalancing") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.elasticloadbalancing[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["elasticloadbalancing"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-elasticloadbalancing" })
}

#######################
# VPC Endpoint for CloudTrail
#######################
data "aws_vpc_endpoint_service" "cloudtrail" {
  count = contains(local.var_enable_endpoints_parsed, "cloudtrail") ? 1 : 0

  service = "cloudtrail"
}

resource "aws_vpc_endpoint" "cloudtrail" {
  count = contains(local.var_enable_endpoints_parsed, "cloudtrail") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.cloudtrail[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["cloudtrail"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-cloudtrail" })
}

#######################
# VPC Endpoint for Kinesis Streams
#######################
data "aws_vpc_endpoint_service" "kinesis_streams" {
  count = contains(local.var_enable_endpoints_parsed, "kinesisstreams") ? 1 : 0

  service = "kinesis-streams"
}

resource "aws_vpc_endpoint" "kinesis_streams" {
  count = contains(local.var_enable_endpoints_parsed, "kinesisstreams") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.kinesis_streams[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["kinesisstreams"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-kinesisstreams" })
}

#######################
# VPC Endpoint for Kinesis Firehose
#######################
data "aws_vpc_endpoint_service" "kinesis_firehose" {
  count = contains(local.var_enable_endpoints_parsed, "kinesisfirehose") ? 1 : 0

  service = "kinesis-firehose"
}

resource "aws_vpc_endpoint" "kinesis_firehose" {
  count = contains(local.var_enable_endpoints_parsed, "kinesisfirehose") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.kinesis_firehose[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["kinesisfirehose"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-kinesisfirehose" })
}

#######################
# VPC Endpoint for Glue
#######################
data "aws_vpc_endpoint_service" "glue" {
  count = contains(local.var_enable_endpoints_parsed, "glue") ? 1 : 0

  service = "glue"
}

resource "aws_vpc_endpoint" "glue" {
  count = contains(local.var_enable_endpoints_parsed, "glue") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.glue[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["glue"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-glue" })
}

######################################
# VPC Endpoint for Sagemaker Notebooks
######################################
data "aws_vpc_endpoint_service" "sagemaker_notebook" {
  count = contains(local.var_enable_endpoints_parsed, "notebook") ? 1 : 0

  service = "notebook"
}

resource "aws_vpc_endpoint" "sagemaker_notebook" {
  count = contains(local.var_enable_endpoints_parsed, "notebook") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.sagemaker_notebook[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["notebook"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-sagemakernotebook" })
}

#######################
# VPC Endpoint for STS
#######################
data "aws_vpc_endpoint_service" "sts" {
  count = contains(local.var_enable_endpoints_parsed, "sts") ? 1 : 0

  service = "sts"
}

resource "aws_vpc_endpoint" "sts" {
  count = contains(local.var_enable_endpoints_parsed, "sts") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.sts[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["sts"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-sts" })
}

#############################
# VPC Endpoint for Cloudformation
#############################
data "aws_vpc_endpoint_service" "cloudformation" {
  count = contains(local.var_enable_endpoints_parsed, "cloudformation") ? 1 : 0

  service = "cloudformation"
}

resource "aws_vpc_endpoint" "cloudformation" {
  count = contains(local.var_enable_endpoints_parsed, "cloudformation") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.cloudformation[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["cloudformation"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-cloudformation" })
}

#############################
# VPC Endpoint for CodePipeline
#############################
data "aws_vpc_endpoint_service" "codepipeline" {
  count = contains(local.var_enable_endpoints_parsed, "codepipeline") ? 1 : 0

  service = "codepipeline"
}

resource "aws_vpc_endpoint" "codepipeline" {
  count = contains(local.var_enable_endpoints_parsed, "codepipeline") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.codepipeline[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["codepipeline"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-codepipeline" })
}

#############################
# VPC Endpoint for AppMesh
#############################
data "aws_vpc_endpoint_service" "appmesh_envoy_management" {
  count = contains(local.var_enable_endpoints_parsed, "appmeshenvoymanagement") ? 1 : 0

  service = "appmesh-envoy-management"
}

resource "aws_vpc_endpoint" "appmesh_envoy_management" {
  count = contains(local.var_enable_endpoints_parsed, "appmeshenvoymanagement") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.appmesh_envoy_management[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["appmeshenvoymanagement"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-appmeshenvoymanagement" })
}

#############################
# VPC Endpoint for Service Catalog
#############################
data "aws_vpc_endpoint_service" "servicecatalog" {
  count = contains(local.var_enable_endpoints_parsed, "servicecatalog") ? 1 : 0

  service = "servicecatalog"
}

resource "aws_vpc_endpoint" "servicecatalog" {
  count = contains(local.var_enable_endpoints_parsed, "servicecatalog") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.servicecatalog[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["servicecatalog"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-servicecatalog" })
}

#############################
# VPC Endpoint for Storage Gateway
#############################
data "aws_vpc_endpoint_service" "storagegateway" {
  count = contains(local.var_enable_endpoints_parsed, "storagegateway") ? 1 : 0

  service = "storagegateway"
}

resource "aws_vpc_endpoint" "storagegateway" {
  count = contains(local.var_enable_endpoints_parsed, "storagegateway") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.storagegateway[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["storagegateway"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-storagegateway" })
}

#############################
# VPC Endpoint for Transfer
#############################
data "aws_vpc_endpoint_service" "transfer" {
  count = contains(local.var_enable_endpoints_parsed, "transfer") ? 1 : 0

  service = "transfer"
}

resource "aws_vpc_endpoint" "transfer" {
  count = contains(local.var_enable_endpoints_parsed, "transfer") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.transfer[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["transfer"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-transfer" })
}

#############################
# VPC Endpoint for SageMaker API
#############################
data "aws_vpc_endpoint_service" "sagemaker_api" {
  count = contains(local.var_enable_endpoints_parsed, "sagemakerapi") ? 1 : 0

  service = "sagemaker.api"
}

resource "aws_vpc_endpoint" "sagemaker_api" {
  count = contains(local.var_enable_endpoints_parsed, "sagemakerapi") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.sagemaker_api[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["sagemakerapi"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-sagemakerapi" })
}

#############################
# VPC Endpoint for SageMaker Runtime
#############################
data "aws_vpc_endpoint_service" "sagemaker_runtime" {
  count = contains(local.var_enable_endpoints_parsed, "sagemakerruntime") ? 1 : 0

  service = "sagemaker.runtime"
}

resource "aws_vpc_endpoint" "sagemaker_runtime" {
  count = contains(local.var_enable_endpoints_parsed, "sagemakerruntime") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.sagemaker_runtime[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["sagemakerruntime"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-sagemakerruntime" })
}

#############################
# VPC Endpoint for AppStream API
#############################
data "aws_vpc_endpoint_service" "appstream_api" {
  count = contains(local.var_enable_endpoints_parsed, "appstreamapi") ? 1 : 0

  service = "appstream.api"
}

resource "aws_vpc_endpoint" "appstream_api" {
  count = contains(local.var_enable_endpoints_parsed, "appstreamapi") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.appstream_api[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["appstreamapi"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-appstreamapi" })
}

#############################
# VPC Endpoint for AppStream STREAMING
#############################
data "aws_vpc_endpoint_service" "appstream_streaming" {
  count = contains(local.var_enable_endpoints_parsed, "appstreamstreaming") ? 1 : 0

  service = "appstream.streaming"
}

resource "aws_vpc_endpoint" "appstream_streaming" {
  count = contains(local.var_enable_endpoints_parsed, "appstreamstreaming") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.appstream_streaming[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["appstreamstreaming"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-appstreamstreaming" })
}

#############################
# VPC Endpoint for Athena
#############################
data "aws_vpc_endpoint_service" "athena" {
  count = contains(local.var_enable_endpoints_parsed, "athena") ? 1 : 0

  service = "athena"
}

resource "aws_vpc_endpoint" "athena" {
  count = contains(local.var_enable_endpoints_parsed, "athena") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.athena[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["athena"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-athena" })
}

#############################
# VPC Endpoint for Rekognition
#############################
data "aws_vpc_endpoint_service" "rekognition" {
  count = contains(local.var_enable_endpoints_parsed, "rekognition") ? 1 : 0

  service = "rekognition"
}

resource "aws_vpc_endpoint" "rekognition" {
  count = contains(local.var_enable_endpoints_parsed, "rekognition") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.rekognition[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["rekognition"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-rekognition" })
}

#######################
# VPC Endpoint for EFS
#######################
data "aws_vpc_endpoint_service" "efs" {
  count = contains(local.var_enable_endpoints_parsed, "elasticfilesystem") ? 1 : 0

  service = "elasticfilesystem"
}

resource "aws_vpc_endpoint" "efs" {
  count = contains(local.var_enable_endpoints_parsed, "elasticfilesystem") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.efs[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["elasticfilesystem"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-elasticfilesystem" })
}

#######################
# VPC Endpoint for Cloud Directory
#######################
data "aws_vpc_endpoint_service" "cloud_directory" {
  count = contains(local.var_enable_endpoints_parsed, "clouddirectory") ? 1 : 0

  service = "clouddirectory"
}

resource "aws_vpc_endpoint" "cloud_directory" {
  count = contains(local.var_enable_endpoints_parsed, "clouddirectory") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.cloud_directory[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["clouddirectory"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-clouddirectory" })
}

#######################
# VPC Endpoint for Auto Scaling Plans
#######################
data "aws_vpc_endpoint_service" "auto_scaling_plans" {
  count = contains(local.var_enable_endpoints_parsed, "autoscalingplans") ? 1 : 0

  service = "autoscaling-plans"
}

resource "aws_vpc_endpoint" "auto_scaling_plans" {
  count = contains(local.var_enable_endpoints_parsed, "autoscalingplans") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.auto_scaling_plans[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["autoscalingplans"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-autoscalingplans" })
}

#######################
# VPC Endpoint for Workspaces
#######################
data "aws_vpc_endpoint_service" "workspaces" {
  count = contains(local.var_enable_endpoints_parsed, "workspaces") ? 1 : 0

  service = "workspaces"
}

resource "aws_vpc_endpoint" "workspaces" {
  count = contains(local.var_enable_endpoints_parsed, "workspaces") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.workspaces[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["workspaces"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-workspaces" })
}

#######################
# VPC Endpoint for Access Analyzer
#######################
data "aws_vpc_endpoint_service" "access_analyzer" {
  count = contains(local.var_enable_endpoints_parsed, "accessanalyzer") ? 1 : 0

  service = "access-analyzer"
}

resource "aws_vpc_endpoint" "access_analyzer" {
  count = contains(local.var_enable_endpoints_parsed, "accessanalyzer") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.access_analyzer[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["accessanalyzer"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-accessanalyzer" })
}

#######################
# VPC Endpoint for EBS
#######################
data "aws_vpc_endpoint_service" "ebs" {
  count = contains(local.var_enable_endpoints_parsed, "ebs") ? 1 : 0

  service = "ebs"
}

resource "aws_vpc_endpoint" "ebs" {
  count = contains(local.var_enable_endpoints_parsed, "ebs") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.ebs[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["ebs"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-ebs" })
}

#######################
# VPC Endpoint for Data Sync
#######################
data "aws_vpc_endpoint_service" "datasync" {
  count = contains(local.var_enable_endpoints_parsed, "datasync") ? 1 : 0

  service = "datasync"
}

resource "aws_vpc_endpoint" "datasync" {
  count = contains(local.var_enable_endpoints_parsed, "datasync") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.datasync[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["datasync"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-datasync" })
}

#######################
# VPC Endpoint for Elastic Inference Runtime
#######################
data "aws_vpc_endpoint_service" "elastic_inference_runtime" {
  count = contains(local.var_enable_endpoints_parsed, "elasticinferenceruntime") ? 1 : 0

  service = "elastic-inference.runtime"
}

resource "aws_vpc_endpoint" "elastic_inference_runtime" {
  count = contains(local.var_enable_endpoints_parsed, "elasticinferenceruntime") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.elastic_inference_runtime[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["elasticinferenceruntime"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-elasticinferenceruntime" })
}

#######################
# VPC Endpoint for SMS
#######################
data "aws_vpc_endpoint_service" "sms" {
  count = contains(local.var_enable_endpoints_parsed, "sms") ? 1 : 0

  service = "sms"
}

resource "aws_vpc_endpoint" "sms" {
  count = contains(local.var_enable_endpoints_parsed, "sms") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.sms[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["sms"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-sms" })
}

#######################
# VPC Endpoint for EMR
#######################
data "aws_vpc_endpoint_service" "emr" {
  count = contains(local.var_enable_endpoints_parsed, "elasticmapreduce") ? 1 : 0

  service = "elasticmapreduce"
}

resource "aws_vpc_endpoint" "emr" {
  count = contains(local.var_enable_endpoints_parsed, "elasticmapreduce") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.emr[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["elasticmapreduce"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-elasticmapreduce" })
}

#######################
# VPC Endpoint for QLDB Session
#######################
data "aws_vpc_endpoint_service" "qldb_session" {
  count = contains(local.var_enable_endpoints_parsed, "qldbsession") ? 1 : 0

  service = "qldb.session"
}

resource "aws_vpc_endpoint" "qldb_session" {
  count = contains(local.var_enable_endpoints_parsed, "qldbsession") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.qldb_session[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["qldbsession"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-qldbsession" })
}

#############################
# VPC Endpoint for Step Function
#############################
data "aws_vpc_endpoint_service" "states" {
  count = contains(local.var_enable_endpoints_parsed, "states") ? 1 : 0

  service = "states"
}

resource "aws_vpc_endpoint" "states" {
  count = contains(local.var_enable_endpoints_parsed, "states") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.states[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["states"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-states" })
}

#############################
# VPC Endpoint for Elastic Beanstalk
#############################
data "aws_vpc_endpoint_service" "elasticbeanstalk" {
  count = contains(local.var_enable_endpoints_parsed, "elasticbeanstalk") ? 1 : 0

  service = "elasticbeanstalk"
}

resource "aws_vpc_endpoint" "elasticbeanstalk" {
  count = contains(local.var_enable_endpoints_parsed, "elasticbeanstalk") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.elasticbeanstalk[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["elasticbeanstalk"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-elasticbeanstalk" })
}

#############################
# VPC Endpoint for Elastic Beanstalk Health
#############################
data "aws_vpc_endpoint_service" "elasticbeanstalk_health" {
  count = contains(local.var_enable_endpoints_parsed, "elasticbeanstalkhealth") ? 1 : 0

  service = "elasticbeanstalk-health"
}

resource "aws_vpc_endpoint" "elasticbeanstalk_health" {
  count = contains(local.var_enable_endpoints_parsed, "elasticbeanstalkhealth") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.elasticbeanstalk_health[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["elasticbeanstalkhealth"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-elasticbeanstalkhealth" })
}

#############################
# VPC Endpoint for ACM PCA
#############################
data "aws_vpc_endpoint_service" "acm_pca" {
  count = contains(local.var_enable_endpoints_parsed, "acmpca") ? 1 : 0

  service = "acm-pca"
}

resource "aws_vpc_endpoint" "acm_pca" {
  count = contains(local.var_enable_endpoints_parsed, "acmpca") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.acm_pca[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["acmpca"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-acmpca" })
}

#######################
# VPC Endpoint for SES
#######################
data "aws_vpc_endpoint_service" "ses" {
  count = contains(local.var_enable_endpoints_parsed, "emailsmtp") ? 1 : 0

  service = "email-smtp"
}

resource "aws_vpc_endpoint" "ses" {
  count = contains(local.var_enable_endpoints_parsed, "emailsmtp") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.ses[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["emailsmtp"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-emailsmtp" })
}

######################
# VPC Endpoint for RDS
######################
data "aws_vpc_endpoint_service" "rds" {
  count = contains(local.var_enable_endpoints_parsed, "rds") ? 1 : 0

  service = "rds"
}

resource "aws_vpc_endpoint" "rds" {
  count = contains(local.var_enable_endpoints_parsed, "rds") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.rds[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["rds"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-rds" })
}

#############################
# VPC Endpoint for CodeDeploy
#############################
data "aws_vpc_endpoint_service" "codedeploy" {
  count = contains(local.var_enable_endpoints_parsed, "codedeploy") ? 1 : 0

  service = "codedeploy"
}

resource "aws_vpc_endpoint" "codedeploy" {
  count = contains(local.var_enable_endpoints_parsed, "codedeploy") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.codedeploy[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["codedeploy"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-codedeploy" })
}

#############################################
# VPC Endpoint for CodeDeploy Commands Secure
#############################################
data "aws_vpc_endpoint_service" "codedeploy_commands_secure" {
  count = contains(local.var_enable_endpoints_parsed, "codedeploycommandssecure") ? 1 : 0

  service = "codedeploy-commands-secure"
}

resource "aws_vpc_endpoint" "codedeploy_commands_secure" {
  count = contains(local.var_enable_endpoints_parsed, "codedeploycommandssecure") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.codedeploy_commands_secure[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["codedeploycommandssecure"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-codedeploycommandssecure" })
}

#############################################
# VPC Endpoint for Textract
#############################################
data "aws_vpc_endpoint_service" "textract" {
  count = contains(local.var_enable_endpoints_parsed, "textract") ? 1 : 0

  service = "textract"
}

resource "aws_vpc_endpoint" "textract" {
  count = contains(local.var_enable_endpoints_parsed, "textract") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.textract[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["textract"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-textract" })
}

#############################################
# VPC Endpoint for Codeartifact API
#############################################
data "aws_vpc_endpoint_service" "codeartifact_api" {
  count = contains(local.var_enable_endpoints_parsed, "codeartifactapi") ? 1 : 0

  service = "codeartifact.api"
}

resource "aws_vpc_endpoint" "codeartifact_api" {
  count = contains(local.var_enable_endpoints_parsed, "codeartifactapi") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.codeartifact_api[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["codeartifactapi"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-codeartifactapi" })
}

#############################################
# VPC Endpoint for Codeartifact repositories
#############################################
data "aws_vpc_endpoint_service" "codeartifact_repositories" {
  count = contains(local.var_enable_endpoints_parsed, "codeartifactrepositories") ? 1 : 0

  service = "codeartifact.repositories"
}

resource "aws_vpc_endpoint" "codeartifact_repositories" {
  count = contains(local.var_enable_endpoints_parsed, "codeartifactrepositories") ? 1 : 0

  vpc_id            = aws_vpc.this[0].id
  service_name      = data.aws_vpc_endpoint_service.codeartifact_repositories[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.default["codeartifactrepositories"].id]
  subnet_ids          = aws_subnet.private.*.id
  private_dns_enabled = var.enable_ep_private_dns
  tags                = merge(local.tags, { Name = "${var.name}-endpoint-codeartifactrepositories" })
}
