resource "aws_ram_resource_share" "vpc_share" {
  count = length(var.share_private_subnets_with_accounts) > 0 ? 1 : 0

  name = lower("${var.name}-${data.aws_region.current.name}-share-tflz")

  tags = merge(local.tags, {
    Name = lower("${var.name}-${data.aws_region.current.name}-share-tflz")
  })
}

resource "aws_ram_principal_association" "vpc_share_principal_association" {
  count = length(var.share_private_subnets_with_accounts)

  principal = element(var.share_private_subnets_with_accounts, count.index)

  resource_share_arn = aws_ram_resource_share.vpc_share.0.arn

}

resource "aws_ram_resource_association" "vpc_share_resource_association" {

  count = length(var.share_private_subnets_with_accounts) > 0 && length(aws_subnet.private) > 0 ? length(aws_subnet.private) : 0

  resource_arn       = element(aws_subnet.private, count.index).arn
  resource_share_arn = aws_ram_resource_share.vpc_share.0.arn
}
